class git::daemon::base {

  file { 'git-daemon_initscript':
    source  => [ "puppet:///modules/site_git/init.d/${::fqdn}/git-daemon",
                 "puppet:///modules/site_git/init.d/${::operatingsystem}/git-daemon",
                 'puppet:///modules/site_git/init.d/git-daemon',
                 "puppet:///modules/git/init.d/${::operatingsystem}/git-daemon",
                 'puppet:///modules/git/init.d/git-daemon' ],
    require => Package['git'],
    path    => '/etc/init.d/git-daemon',
    owner   => root, group => 0, mode => '0755';
  }

  file { 'git-daemon_config':
    source  => [ "puppet:///modules/site_git/config/${::fqdn}/git-daemon",
                 "puppet:///modules/site_git/config/${::operatingsystem}/git-daemon",
                 'puppet:///modules/site_git/config/git-daemon',
                 "puppet:///modules/git/config/${::operatingsystem}/git-daemon",
                 'puppet:///modules/git/config/git-daemon' ],
    require => Package['git'],
    path    => '/etc/default/git-daemon',
    owner   => root, group => 0, mode => '0644';
  }

  service { 'git-daemon':
    ensure    => running,
    enable    => true,
    hasstatus => true,
    require   => [ File['git-daemon_initscript'], File['git-daemon_config'] ],
  }
}
