class git::daemon::disable inherits git::daemon::base {

  if defined(Package['git-daemon']) {
    Package['git-daemon'] {
      ensure => absent,
    }
  }

  Xinetd::File['git']{
    source => 'puppet:///modules/git/xinetd.d/git.disabled'
  }

  File['git-daemon_initscript'] {
    ensure => absent,
  }

  File['git-daemon_config'] {
    ensure => absent,
  }

  Service['git-daemon'] {
    ensure => stopped,
    enable => false,
    require => undef,
    before => File['git-daemon_initscript'],
  }

  if hiera('use_shorewall',false) {
    include shorewall::rules::gitdaemon::absent
  }
}


