# basic git-daemon setup
class git::daemon( )
{
  require git

  case $operatingsystem {
    centos: { include git::daemon::centos }
    debian: { include git::daemon::base }
  }

  if $use_shorewall {
    include shorewall::rules::gitdaemon
  }
}
